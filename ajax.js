function loadJSON() {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'database.json', true);
    xhr.send();
    xhr.onreadystatechange = function () {
        if (xhr.status != 200) {
            alert(xhr.status + ': ' + xhr.statusText);
        } else {
            try {
                var json = JSON.parse(xhr.responseText);
            } catch (e) {
                alert("Некорректный ответ " + e.message);
            }
            return json;
        }
    };
}
